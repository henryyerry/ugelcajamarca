<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db'              => array(
        'username'       => 'root',
        'password'       => '',
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=bdroom;host=127.0.0.1',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'navigation'              => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'navigation'      => array(
        'default' => array(
            array(
                'id'    => 'home',
                'label' => 'Inicio',
                'route' => 'home',
                'icon'  => 'glyphicon glyphicon-home'
            ),
            array(
                'id'    => 'page1',
                'label' => 'Administrador',
                'uri'   => '#',
                'icon'  => 'fa fa-wrench fa-fw',
                'pages' => array(
                    array(
                        'label' => 'Seguridad',
                        'uri'   => '#',
                        'icon'  => 'fa fa-lock fa-fw',
                        'pages' => array(
                            array(
                                'label'     => 'Usuarios',
                                'route'     => 'administrador',
                                'action'    => 'index',
                                'resource'  => 'Administrador\Controller\Index',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-users fa-fw'
                            ),
                            array(
                                'label'     => 'Roles y Políticas',
                                'route'     => 'roles',
                                'action'    => 'index',
                                'resource'  => 'Administrador\Controller\Rol',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-legal fa-fw'
                            ),
                        ),
                    ),
                    array(
                        'label' => 'Lugar',
                        'uri'   => '#',
                        'icon'  => 'fa fa-flag-checkered fa-fw',
                        'pages' => array(
                            array(
                                'label'     => 'Distritos',
                                'route'     => 'distrito',
                                'action'    => 'index',
                                'params'    => array('id' => '1'),
                                'resource'  => 'Lugar\Controller\Distrito',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-map-marker fa-fw'
                            ),
                            array(
                                'label'     => 'Lugares',
                                'route'     => 'unidad',
                                'action'    => 'index',
                                'resource'  => 'Lugar\Controller\Unidad',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-map-marker fa-fw'
                            ),
                        ),
                    ),
                    array(
                        'label' => 'Ocupaciones',
                        'uri'   => '#',
                        'icon'  => 'fa fa-flag-checkered fa-fw',
                        'pages' => array(
                            array(
                                'label'     => 'Todas',
                                'route'     => 'ocupacion',
                                'action'    => 'index',
                                'resource'  => 'Ocupacion\Controller\Index',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-users fa-fw'
                            ),
                            array(
                                'label'     => 'Agregar Nuevo',
                                'route'     => 'ocupacion',
                                'action'    => 'add',
                                'resource'  => 'Ocupacion\Controller\Index',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-legal fa-fw'
                            ),
                        ),
                    )
                )
            ),
            array(
                'id'    => 'page2',
                'label' => 'Inventario',
                'uri'   => '#',
                'icon'  => 'fa fa-wrench fa-fw',
                'pages' => array(
                    array(
                        'label' => 'Productos',
                        'uri'   => '#',
                        'icon'  => 'fa fa-lock fa-fw',
                        'pages' => array(
                            array(
                                'label'     => 'Productos',
                                'route'     => 'producto',
                                'action'    => 'index',
                                'resource'  => 'Inventario\Controller\Producto',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-users fa-fw'
                            ),
                            array(
                                'label'     => 'Nuevo Producto',
                                'route'     => 'producto',
                                'action'    => 'add',
                                'resource'  => 'Inventario\Controller\Producto',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-legal fa-fw'
                            ),
                            array(
                                'label'     => 'Reportes Producto',
                                'route'     => 'producto',
                                'action'    => 'reporte',
                                'resource'  => 'Inventario\Controller\Producto',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-legal fa-fw'
                            ),
                        ),
                    ),
                    array(
                        'label' => 'Préstamo',
                        'uri'   => '#',
                        'icon'  => 'fa fa-flag-checkered fa-fw',
                        'pages' => array(
                            array(
                                'label'     => 'Préstamos',
                                'route'     => 'prestamo',
                                'action'    => 'index',
                                'resource'  => 'Inventario\Controller\Prestamo',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-map-marker fa-fw'
                            ),
                            array(
                                'label'     => 'Nuevo Préstamo',
                                'route'     => 'prestamo',
                                'action'    => 'add',
                                'resource'  => 'Inventario\Controller\Prestamo',
                                'privilege' => 'index',
                                'icon'      => 'fa fa-map-marker fa-fw'
                            ),
                        ),
                    ),
                )
            ),
            array(
                'id'    => 'page3',
                'label' => 'Trabajo',
                'uri'   => '#',
                'icon'  => 'glyphicon glyphicon-briefcase',
                'pages' => array(
                    array(
                        'label'     => 'Trabajos',
                        'route'     => 'trabajo',
                        'action'    => 'index',
                        'resource'  => 'Trabajo\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-map-marker fa-fw'
                    ),
                    array(
                        'label'     => 'Agregar Nuevo',
                        'route'     => 'trabajo',
                        'action'    => 'add',
                        'resource'  => 'Trabajo\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-map-marker fa-fw'
                    ),


                )
            ),
            array(
                'id'    => 'page4',
                'label' => 'Personas',
                'uri'   => '#',
                'icon'  => 'fa fa-users fa-fw',
                'pages' => array(
                    array(
                        'label'     => 'Personas',
                        'route'     => 'persona',
                        'action'    => 'index',
                        'resource'  => 'Persona\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-map-marker fa-fw'
                    ),
                    array(
                        'label'     => 'Agregar Nuevo',
                        'route'     => 'persona',
                        'action'    => 'add',
                        'resource'  => 'Persona\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-map-marker fa-fw'
                    ),
                    
                )
            ),
            array(
                'id'    => 'page5',
                'label' => 'Egresos',
                'uri'   => '#',
                'icon'  => 'fa fa-money fa-fw',
                'pages' => array(
                    array(
                        'label'     => 'Gestionar',
                        'route'     => 'egreso',
                        'action'    => 'index',
                        'resource'  => 'Egreso\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-map-marker fa-fw'
                    ),
                )
            ),
            array(
                'id'    => 'page6',
                'label' => 'Eventos',
                'uri'   => '#',
                'icon'  => 'fa fa-calendar fa-fw',
                'pages' => array(
                    array(
                        'label'     => 'Gestionar',
                        'route'     => 'evento',
                        'action'    => 'index',
                        'resource'  => 'Evento\Controller\Index',
                        'privilege' => 'index',
                        'icon'      => 'fa fa-bell fa-fw'
                    ),
                )
            ),

            array(
                'id'    => 'login',
                'label' => 'Ingresar',
                'icon'  => 'glyphicon glyphicon-log-in',
                'route' => 'login',
            ),
            array(
                'id'        => 'logout',
                'label'     => 'Salir',
                'icon'      => 'fa fa-sign-out fa-fw',
                'route'     => 'login',
                'action'    => 'logout',
                'resource'  => 'Autentificacion\Controller\Index',
                'privilege' => 'logout',
            ),


        ),
    ),
);
