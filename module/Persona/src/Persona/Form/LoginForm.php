<?php
namespace Persona\Form;

use Zend\Form\Form;

/**
 *
 */
class LoginForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'login-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(
            array(
                'name' => 'idpersona',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'usuario',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese usuario',
                    'class'          => 'form-control',
                    'id'             => 'usuario',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'Password',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'pass',
                'type'       => 'password',
                'attributes' => array(
                    'placeholder'    => 'Contraseña',
                    'id'             => 'pass',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Contraseña',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'pregunta',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ej: ¿Cúal el nombre de mi mascota?',
                    'id'             => 'pregunta',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Pregunta para recuperar contraseña',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'respuesta',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Respuesta a la pregunta clave',
                    'id'             => 'respuesta',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Respuesta a la pregunta anterior',
                ),
            )
        );

        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '1' => 'habilitado',
                        '2' => 'Deshabilitado'
                    ),
                ),
                'attributes' => array(
//                    'value' => '1',
                ),
            )
        );
    }
}