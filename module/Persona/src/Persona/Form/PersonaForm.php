<?php
namespace Persona\Form;

use Zend\Form\Form;

/**
 *
 */
class PersonaForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'persona-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(
            array(
                'name' => 'idpersona',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'dni',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese DNI',
                    'class'          => 'form-control',
                    'id'             => 'dni',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'DNI',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'nombre',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese el nombre',
                    'id'             => 'nombre',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Nombre del cliente',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'apellidopaterno',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'El apellido paterno',
                    'id'             => 'apellidopaterno',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Apellido paterno del cliente',
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'apellidomaterno',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'El apellido materno',
                    'id'             => 'apellidomaterno',
                    'class'          => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Apellido materno del materno',
                ),
            )
        );

        $this->add(
            array(
                'name'       => 'telefono',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese Telefóno',
                    'class'          => 'form-control',
                    'id'             => 'telefono',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'Teléfono',
                ),
            )
        );

        $this->add(
            array(
                'name'       => 'fechanacimiento',
                'type'       => 'Text',
                'options'    => array(
                    'format' => 'd/m/Y'
                ),
                'attributes' => array(
                    'class'       => 'form-control',
                    'id'          => 'fechanaciemiento',
                    'placeholder' => 'YYYY/MM/DD',
                    'title'       => 'Fecha Nacimiento',
                    'min'         => '1930-01-01',
                    'max'         => '2020-01-01',
                    'step'        => '1',
                    'data-plugin' => 'datepicker'
                )
            )
        );

        $this->add(
            array(
                'name'       => 'direccion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder'    => 'Ingrese Dirección',
                    'class'          => 'form-control',
                    'id'             => 'direccion',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'Dirección',
                ),
            )
        );

        $this->add(
            array(
                'name'       => 'correo',
                'type'       => 'Text',
                'attributes' => array(
                    'placeholder'    => 'Ingrese Email',
                    'class'          => 'form-control',
                    'id'             => 'correo',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'Email'
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'Textarea',
                'attributes' => array(
                    'placeholder' => 'Ingrese una descripción',
                    'id'          => 'descripcion',
                    'class'       => 'form-control',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Detalle el control para poder utilizarlo correctamente',
                ),
            )
        );
        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'disable_inarray_validator' => true, // <-- disable
                    'label_attributes' => array(
                        'class' => 'radio-inline',
                    ),
                    'value_options'    => array(
                        '1' => 'Implementado',
                        '2' => 'Por implementar'
                    ),
                ),
                'attributes' => array(
//                    'value' => '1',
                ),
            )
        );
    }
}