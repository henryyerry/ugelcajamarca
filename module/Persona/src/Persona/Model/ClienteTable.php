<?php
namespace Persona\Model;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

/**
 *
 */
class ClienteTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    public function getCliente($id){
        $rowset = $this->tableGateway->select(array('idcliente' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function getExisteDni($dni)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array('idcliente'=>'idcliente')
        );
        $sqlSelect
            ->join(
                'persona',
                'persona.idpersona = cliente.idcliente',
                array(
                )
            );
        $sqlSelect->where(
            array(
                "persona.dni='$dni'",
            )
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    public function getClienteLogin()
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'idcliente'=>'idcliente',
                'estadocliente'=>'estado'
            )
        );
        $sqlSelect
            ->join(
                'persona',
                'persona.idpersona = cliente.idcliente',
                array(
                    'nombre'=>'nombre',
                    'apellidopaterno'=>'apellidopaterno',
                    'apellidomaterno'=>'apellidomaterno',
                    'dni'=>'dni',
                    'estado'=>'estado'
                )
            )->join(
                'login',
                'login.idpersona = persona.idpersona',
                array(
                    'usuario'=>'usuario',
                    'idpersona'=>'idpersona'
                )
            );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();
        return $resultSet;
    }
    public function saveCliente(Cliente $persona)
    {
        $data = array(
            'idcliente'=>$persona->idcliente,
            'estado'=>$persona->estado
        );
        $id = (int) $persona->idcliente;
        if ($this->getCliente($id)) {
            $this->tableGateway->update($data, array('idcliente' => $id));
        } else {
            $this->tableGateway->insert($data);
            $id=$this->tableGateway->lastInsertValue;
        }
        return $id;
    }

    public function deleteCliente($id)
    {
        $this->tableGateway->delete(array('idcliente' => $id));
    }


}

?>