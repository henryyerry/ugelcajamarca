<?php 
namespace Persona\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * 
 */
 class PersonaTable
 {
     protected $tableGateway;
    protected $dbAdapter;

 	public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

 	public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }
     public function getPersona($id){
     	 $rowset = $this->tableGateway->select(array('idpersona' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("No se encontro datos en: $id");
         }
         return $row;
     }

     public function enablePersona($id){
        $this->tableGateway->update(array("estado"=>"1"),array('idpersona' => $id));
     }

    public function disablePersona($id){
        $this->tableGateway->update(array("estado"=>"0"),array('idpersona' => $id));
     }
     public function getApellido($apellido)
     {
         $sqlSelect = $this->tableGateway->getSql()->select();
         $sqlSelect->columns(
             array(
                 'idpersona'=>'idpersona',
                 'dni'=>'dni',
                 'nombre'=>'nombre',
                 'apellidopaterno'=>'apellidopaterno',
                 'apellidomaterno'=>'apellidomaterno',
                 'estadopersona'=>'estado',
                 'telefono'=>'telefono',
             )
         );
         $sqlSelect
             ->join(
                 'cliente',
                 'cliente.idcliente = persona.idpersona',
                 array(
                     'estadocliente'=>'estado'
                 )
             );
         $sqlSelect->where(
             array(
                 "persona.apellidopaterno='$apellido' OR persona.apellidomaterno='$apellido'",
             )
         );
         $statement = $this->tableGateway->getSql()
             ->prepareStatementForSqlObject($sqlSelect);
         $resultSet = $statement->execute();
         return $resultSet;
     }
     public function getDni($dni)
     {
         $sqlSelect = $this->tableGateway->getSql()->select();
         $sqlSelect->columns(
             array(
                 'idpersona'=>'idpersona',
                 'dni'=>'dni',
                 'nombre'=>'nombre',
                 'apellidopaterno'=>'apellidopaterno',
                 'apellidomaterno'=>'apellidomaterno',
                 'estadopersona'=>'estado',
                 'telefono'=>'telefono',
             )
         );
         $sqlSelect
             ->join(
                 'cliente',
                 'cliente.idcliente = persona.idpersona',
                 array(
                     'estadocliente'=>'estado'
                 )
             );
         $sqlSelect->where->like('persona.dni', $dni.'%'
         );
         $statement = $this->tableGateway->getSql()
             ->prepareStatementForSqlObject($sqlSelect);
         $resultSet = $statement->execute();
         return $resultSet;
     }
     public function getUsuarioLogin()
     {
         $sqlSelect = $this->tableGateway->getSql()->select();
         $sqlSelect->columns(
             array(
                 'idusuario'=>'idusuario'
             )
         );
         $sqlSelect
             ->join(
                 'login',
                 'login.idpersona = persona.idpersona',
                 array(
                 )
             );

         $statement = $this->tableGateway->getSql()
             ->prepareStatementForSqlObject($sqlSelect);
         $resultSet = $statement->execute();
         return $resultSet;
     }
     public function getUsuario($user)
     {
         $sqlSelect = $this->tableGateway->getSql()->select();
         $sqlSelect->columns(
             array(
                 'idpersona'=>'idpersona',
                 'nombre'=>'nombre',
                 'apellidopaterno'=>'apellidopaterno',
             )
         );
         $sqlSelect
             ->join(
                 'login',
                 'login.idpersona = persona.idpersona',
                 array(
                     'pass'=>('pass'),
                 )
             );
         $sqlSelect->where(
             array(
                 "login.usuario='$user'",
             )
         );

         $statement = $this->tableGateway->getSql()
             ->prepareStatementForSqlObject($sqlSelect);
         $resultSet = $statement->execute();
         return $resultSet;
     }
     public function savePersona(Persona $persona)
     {
         $data = array(
             'dni'  => $persona->dni,
             'nombre'  => $persona->nombre,
             'apellidopaterno'  => $persona->apellidopaterno,
             'apellidomaterno'  => $persona->apellidomaterno,
             'fechanacimiento'  => $persona->fechanacimiento,
             'direccion'  => $persona->direccion,
             'correo'  => $persona->correo,
             'telefono'  => $persona->telefono,
             'descripcion' => $persona->descripcion,
             'estado' => $persona->estado,
         );

         $id = (int) $persona->idpersona;
         if ($id == 0) {
             $this->tableGateway->insert($data);
             $id=$this->tableGateway->lastInsertValue;
         } else {
             if ($this->getPersona($id)) {
                 $this->tableGateway->update($data, array('idpersona' => $id));
             } else {
                 throw new \Exception('Persona no existe');
             }
         }
         return $id;
     }

     public function deletePersona($id)
     {
         $this->tableGateway->delete(array('idpersona' => $id));
     }


 } 

 ?>