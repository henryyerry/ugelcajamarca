<?php
namespace Persona\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Usuario implements InputFilterAwareInterface
{
    public $idusuario;
    public $jefe;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idusuario = (!empty($data['idusuario'])) ? $data['idusuario']
            : null;
        $this->jefe = (!empty($data['jefe'])) ? $data['jefe']
            : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idusuario',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'     => 'jefe',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
