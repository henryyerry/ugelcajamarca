<?php
namespace Persona\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Login implements InputFilterAwareInterface
{
    public $usuario;
    public $pass;
    public $idpersona;
    public $pregunta;
    public $respuesta;
    public $intentos;
    public $estado;
    protected $inputFilter;
    public function exchangeArray($data)
    {
        $this->usuario = (!empty($data['usuario'])) ? $data['usuario']
            : null;
        $this->pass = (!empty($data['pass'])) ? $data['pass']
            : null;
        $this->idpersona = (!empty($data['idpersona'])) ? $data['idpersona']
            : null;
        $this->pregunta = (!empty($data['pregunta'])) ? $data['pregunta']
            : null;
        $this->respuesta = (!empty($data['respuesta'])) ? $data['respuesta']
            : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado']
            : 1;
        $this->intentos = (!empty($data['intentos'])) ? $data['intentos']
            : 5;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idcliente',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'     => 'idpersona',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'     => 'intentos',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'pass',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La contraseña tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La contraseña no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una contraseña"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'pregunta',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La pregunta tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La pregunta no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una pregunta"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'respuesta',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La respuesta tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La respuesta no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una respuesta"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'estado',
                    'required'   => true,
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Elija un control"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
