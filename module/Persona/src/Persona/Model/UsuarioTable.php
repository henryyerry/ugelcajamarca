<?php
namespace Persona\Model;


use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
/**
 *
 */
class UsuarioTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    public function getUsuario($id){
        $rowset = $this->tableGateway->select(array('idusuario' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }
    public function getUsuarioLogin()
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'idusuario'=>'idusuario'
            )
        );
        $sqlSelect
            ->join(
                'persona',
                'persona.idpersona = usuario.idusuario',
                array(
                    'nombre'=>'nombre',
                    'apellidopaterno'=>'apellidopaterno',
                    'apellidomaterno'=>'apellidomaterno',
                    'dni'=>'dni',
                )
            )->join(
                'login',
                'login.idpersona = persona.idpersona',
                array(
                    'usuario'=>'usuario',
                    'idpersona'=>'idpersona'
                )
            );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    public function getExisteDni($dni)
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array(
                'idusuario'=>'idusuario'
            )
        );
        $sqlSelect
            ->join(
                'persona',
                'persona.idpersona = usuario.idusuario',
                array(
                )
            );
        $sqlSelect->where(
            array(
                "persona.dni='$dni'",
            )
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    public function saveUsuario(Usuario $persona)
    {
        $data = array(
            'idusuario'    =>$persona->idusuario,
            'jefe'  => $persona->jefe
        );
        $id = (int) $persona->idusuario;

            if ($this->getUsuario($id)) {
                $this->tableGateway->update($data, array('idusuario' => $id));
            } else {

                $this->tableGateway->insert($data);
                $id=$this->tableGateway->lastInsertValue;
            }

        return $id;
    }

    public function deleteUsuario($id)
    {
        $this->tableGateway->delete(array('idusuario' => $id));
    }


}

?>