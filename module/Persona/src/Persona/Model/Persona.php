<?php
namespace Persona\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Persona implements InputFilterAwareInterface
{
    public $idpersona;
    public $dni;
    public $nombre;
    public $apellidopaterno;
    public $apellidomaterno;
    public $fechanacimiento;
    public $direccion;
    public $correo;
    public $telefono;
    public $estado;
    public $descripcion;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idpersona = (!empty($data['idpersona'])) ? $data['idpersona']
            : null;
        $this->dni = (!empty($data['dni'])) ? $data['dni']
            : null;
        $this->nombre = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->apellidopaterno = (!empty($data['apellidopaterno'])) ? $data['apellidopaterno'] : null;
        $this->apellidomaterno = (!empty($data['apellidomaterno'])) ? $data['apellidomaterno'] : null;
        $this->fechanacimiento = (!empty($data['fechanacimiento'])) ? $data['fechanacimiento'] : null;
        $this->direccion = (!empty($data['direccion'])) ? $data['direccion'] : null;
        $this->correo = (!empty($data['correo'])) ? $data['correo'] : null;
        $this->telefono = (!empty($data['telefono'])) ? $data['telefono'] : null;
        $this->descripcion = (!empty($data['descripcion']))
            ? $data['descripcion'] : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idpersona',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'dni',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 8,
                                'max'      => 8,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El dni tiene que contener 8 caracteres',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El dni no puede superar los 8 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un nombre"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'nombre',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El nombre tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El nombre no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un nombre"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'apellidopaterno',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El apellido tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El apellido no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un apellido"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'apellidomaterno',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'El apellido tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'El apellido no puede superar los 50 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un apellido"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'direccion',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',

                            ),
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'correo',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',

                            ),
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'telefono',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                            ),
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'descripcion',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                                'messages' => array(
                                    \Zend\Validator\StringLength::TOO_SHORT => 'La descripción tiene que contener un caracter',
                                    \Zend\Validator\StringLength::TOO_LONG  => 'La descripción no puede superar los 100 caracteres',
                                ),
                            ),
                        ),
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese una descripción"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'estado',
                    'required'   => true,
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Elija un control"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
