<?php
namespace Persona\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 *
 */
class Cliente implements InputFilterAwareInterface
{
    public $idcliente;
    public $estado;
    protected $inputFilter;


    public function exchangeArray($data)
    {
        $this->idcliente = (!empty($data['idcliente'])) ? $data['idcliente']
            : null;
        $this->estado = (!empty($data['estado'])) ? $data['estado']
            : 1;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idcliente',
                    'required' => false,
                    'filters'  => array(
                        array('name' => 'Int'),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'estado',
                    'required'   => true,
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Elija un control"
                                ),
                            )
                        ),
                    ),
                )
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}
