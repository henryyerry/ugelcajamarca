<?php 
namespace Persona\Model;

use Persona\Form\LoginForm;
use Zend\Db\TableGateway\TableGateway;

/**
 * 
 */
 class LoginTable
 {
     protected $tableGateway;
    protected $dbAdapter;

 	public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

 	public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }
     public function getLogin($id){
     	 $rowset = $this->tableGateway->select(array('usuario' => $id));
         $row = $rowset->current();
         if (!$row) {
             return false;
         }
         return $row;
     }

     public function enableLogin($id){
        $this->tableGateway->update(array("estado"=>"1"),array('usuario' => $id));
     }

    public function disableLogin($id){
        $this->tableGateway->update(array("estado"=>"2"),array('usuario' => $id));
     }

     public function saveLogin(Login $login)
     {
         $data = array(
             'usuario'=>$login->usuario,
             'pass'  => MD5($login->pass),
             'idpersona'  => $login->idpersona,
             'pregunta'  => $login->pregunta,
             'respuesta'  => $login->respuesta,
             'estado'  => $login->estado,
             'intentos'  => $login->intentos,
         );
         $id = $login->usuario;
         if ($this->getLogin($id))
         {
             $this->tableGateway->update($data, array('usuario' => $id));
         } else {
             $this->tableGateway->insert($data);
             $id=$this->tableGateway->lastInsertValue;
         }
         return $id;
     }

     public function deleteUsuario($id)
     {
         $this->tableGateway->delete(array('usuario' => $id));
     }


 } 

 ?>