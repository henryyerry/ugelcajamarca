<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Persona\Controller;

use Persona\Form\LoginForm;
use Persona\Form\PersonaForm;
use Persona\Model\Cliente;
use Persona\Model\Persona;
use Persona\Model\Usuario;
use Zend\Mail\Protocol\Smtp\Auth\Login;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class IndexController extends AbstractActionController
{



    public function indexAction()
    {

        return new ViewModel();
    }


}
