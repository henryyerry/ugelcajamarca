<?php
namespace Habitacion\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
class MultimediaTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $this->resultToArray($resultSet);
    }
	public function fetchAportesPersona($id)
    {
        //$resultSet = $this->tableGateway->select(array('idpersona' => $id, 'estado' => '1'));
	    $resultSet = $this ->tableGateway->select (function (\Zend\Db\Sql\Select $select) {
		    $select -> columns(
			    array(
				    'idaporte',
				    'idpersona',
				    'valor',
				    'fechaaporte',
				    'detalle',
				    'tipo' => new Expression(
					    "IF(tipo = 1, 'MONETARIO', 'NO MONETARIO')"
				    ),
			    ));
	    });
        return $this->resultToArray($resultSet);
    }

    public function getMultimedia($id)
    {
        $rowset = $this->tableGateway->select(array('idmultimedia' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }

        return $row;
    }
	public function getListaAporteMes($idPersona, $month){
		$select = new \Zend\Db\Sql\Select ;

		$select-> from(array('ap' => 'aporte'))
			-> columns(
				array(
					'idaporte',
					'idpersona',
					'valor',
					'fechaaporte',
					'detalle',
					'tipo' => new Expression(
						"IF(tipo = 1, 'MONETARIO', 'NO MONETARIO')"
					),
				))
			-> where(
				array(
					"idpersona = ".$idPersona." AND estado = '1' AND MONTH(fechaaporte) = " . $month
				)
			)
		;
		$statement = $this->tableGateway->getSql()
			->prepareStatementForSqlObject($select);
		$resultSet = $statement->execute();
		$data = $this->resultToArray($resultSet);

		return $data;
	}
	public function getMontoAporteMes($idPersona, $month)
	{
		$select = new \Zend\Db\Sql\Select ;

		$select-> from(array('ap' => 'aporte'))
			-> columns(
				array(
					'suma'      => new Expression("IFNULL(SUM(ap.valor), 0)"),
				))
			-> where(
				array(
					"idpersona = ".$idPersona." AND estado = '1' AND MONTH(fechaaporte) = " . $month
				)
			)
		;
		$statement = $this->tableGateway->getSql()
			->prepareStatementForSqlObject($select);
		$resultSet = $statement->execute();
		$data = $this->resultToArray($resultSet);

		return $data[0]['suma'];
	}
	public function getPersonaAporteFechas($fi, $fn)
	{

		$sqlSelect = $this->tableGateway->getSql()->select();
		$sqlSelect->columns(
			array(
				'idpersona',
				'valor',
				'fechapago',
				'fechaaporte',
				'tipo',
				'detalle',
				'estado',
			)
		);
		$sqlSelect
			->join(
				'persona',
				'persona.idpersona = aporte.idpersona',
				array(
					'nombre'        =>'nombre',
					'apellidopaterno'        => 'apellidopaterno',
					'apellidomaterno' => 'apellidomaterno',
				)

			);
		$sqlSelect->where(
			array(
				"aporte.fechapago>='$fi'",
				"aporte.fechapago<='$fn'",
			)
		);
		$statement = $this->tableGateway->getSql()
			->prepareStatementForSqlObject($sqlSelect);
		$resultSet = $statement->execute();
		return $this->resultToArray($resultSet);
	}

	public function saveMultimedia(Multimedia $multimedia)
    {
        $data = array(
            'idhabitacion'             => $multimedia->idhabitacion,
            'descripcion'             => $multimedia->descripcion,
            'tipo'             => $multimedia->tipo,
            'path'             => $multimedia->path,
            'estado'          => $multimedia->estado,
        );
        $id = (int)$multimedia->idmultimedia;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getMultimedia($id)) {
                $this->tableGateway->update(
                    $data, array('idmultimedia' => $id)
                );
            } else {
                //throw new \Exception('Persona no existe');
	            $id = 0;
            }
        }

        return $id;
    }
    private function resultToArray($result)
    {
        $data = array();
        foreach ($result as $value) {
            $data[] = $value;
        }

        return $data;
    }


}

?>