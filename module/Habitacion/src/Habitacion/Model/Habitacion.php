<?php
namespace Habitacion\Model;

use Zend\Form\Element;
use Zend\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Authentication\Validator;

class Habitacion implements InputFilterAwareInterface
{
	public $idhabitacion;
    public $idpersona;
    public $descripcion;
    public $direccion;
    public $npiso;
	public $metros;
    public $estado;
    public $inputFilter;

    public function exchangeArray($data)
    {
	    $this->idhabitacion = (!empty($data['idhabitacion']))
		    ? $data['idhabitacion'] : null;
        $this->idpersona = (!empty($data['idpersona']))
            ? $data['idpersona'] : null;
	    $this->descripcion = (!empty($data['descripcion']))
		    ? $data['descripcion'] : null;
	    $this->direccion = (!empty($data['direccion']))
		    ? $data['direccion'] : null;
	    $this->npiso = (!empty($data['npiso']))
		    ? $data['npiso'] : 1;
	    $this->metros = (!empty($data['metros']))
		    ? $data['metros'] : null;
        $this->estado = (!empty($data['estado']))
            ? $data['estado'] : '1';

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter\InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idhabitacion',
                    'required' => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                )
            );
	        $inputFilter->add(
		        array(
			        'name'     => 'idpersona',
			        'required' => true,
			        'filters'    => array(
				        array('name' => 'StripTags'),
				        array('name' => 'StringTrim'),
			        ),
		        )
	        );


            $inputFilter->add(
                array(
                    'name'       => 'descripcion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese descripcion"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'direccion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese dirección"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'npiso',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'Int'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese nº de piso"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'metros',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'Int'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese metros cuadrados"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'estado',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione un estado"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}