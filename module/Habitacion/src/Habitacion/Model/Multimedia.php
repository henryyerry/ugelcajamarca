<?php
namespace Habitacion\Model;

use Zend\Form\Element;
use Zend\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Authentication\Validator;

class Multimedia implements InputFilterAwareInterface
{
	public $idhabitacion;
    public $idmultimedia;
    public $tipo;
    public $descripcion;
    public $path;
    public $estado;
    public $inputFilter;

    public function exchangeArray($data)
    {
	    $this->idhabitacion = (!empty($data['idhabitacion']))
		    ? $data['idhabitacion'] : null;
        $this->idmultimedia = (!empty($data['idmultimedia']))
            ? $data['idmultimedia'] : null;
	    $this->tipo = (!empty($data['tipo']))
		    ? $data['tipo'] : null;
	    $this->descripcion = (!empty($data['descripcion']))
		    ? $data['descripcion'] : null;
	    $this->path = (!empty($data['path']))
		    ? $data['path'] : null;
        $this->estado = (!empty($data['estado']))
            ? $data['estado'] : '1';

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {}

}