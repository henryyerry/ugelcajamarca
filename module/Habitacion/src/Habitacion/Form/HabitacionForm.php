<?php
namespace Habitacion\Form;

use Zend\Form\Form;
use Zend\InputFilter;
use Zend\Form\Element;
/**
 *
 */
class HabitacionForm extends Form
{

    function __construct($name = null)
    {
        parent::__construct($name = null);

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'habitacion-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(
            array(
                'name' => 'idhabitacion',
                'type' => 'Hidden',
            )
        );        

        $this->add(
            array(
                'name' => 'idpersona',
                'type' => 'Hidden',
            )
        );

        $this->add(
            array(
                'name'       => 'descripcion',
                'type'       => 'textarea',
                'attributes' => array(
                    'id'             => 'descripicon',
                    'class'          => 'materialize-textarea obligatorio',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
					'title' => 'Max 200 carácteres',
                ),
            )
        );
		
		$this->add(
		    array(
			    'name' => 'direccion',
			    'type' => 'textarea',
			    'attributes' => array(
				    'id'             => 'direccion',
					'class'          => 'materialize-textarea obligatorio',
				    'data-toggle'    => 'tooltip',
				    'data-placement' => 'right',
				    'title' => 'Max 100 carácteres',
			    ),

		    ));


		$this->add(
		    array(
			    'name' => 'npiso',
			    'type' => 'number',
			    'attributes' => array(
				    'id'             => 'npiso',
					'class'=>'obligatorio',
					'min'=>'1',
					'max'=>'10',
			    ),

		    ));

		$this->add(
		    array(
			    'name' => 'metros',
			    'type' => 'number',
			    'attributes' => array(
				    'id'             => 'metros',
					'class'=>'obligatorio',
					'min'=>'1',
					'max'=>'300',
			    ),

		    ));


        $this->add(
            array(
                'type'       => 'Zend\Form\Element\Radio',
                'name'       => 'estado',
                'options'    => array(
                    'label_attributes' => array(
                        'class' => 'with-gap',
                    ),
                    'value_options'    => array(
                        '1' => 'Activo',
                        '2' => 'Inactivo',
                    ),
                ),
                'attributes' => array(
                    'id'    => 'estado',
                ),
            )
        );        
    }
}