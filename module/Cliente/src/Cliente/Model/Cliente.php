<?php
namespace Cliente\Model;

use Zend\Form\Element;
use Zend\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Authentication\Validator;

class Cliente implements InputFilterAwareInterface
{
	public $idpersona;
    public function exchangeArray($data)
    {
	    $this->idpersona = (!empty($data['idpersona']))
		    ? $data['idpersona'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    public function getInputFilter()
    {
    }

}