<?php
namespace Cliente\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
class ClienteTable
{
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $this->resultToArray($resultSet);
    }

    public function getCliente($id)
    {
        $rowset = $this->tableGateway->select(array('idpersona' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }

        return $row;
    }

    public function getAsientosBus($idbus)
    {

        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(
            array('idbus',
                'numero',
                'tipo',
                'detalle',
                'fila',
                'columna',
                'estado',
                'idasiento'
            )
        );
        $sqlSelect->where(
            array(
                "asiento.idbus='$idbus'",
            )
        );
        $statement = $this->tableGateway->getSql()
            ->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();

        return $this->resultToArray($resultSet);
    }

	public function saveCliente(Cliente $cliente)
    {
        $data = array(
            'idbus'             => $cliente->idpersona,
        );

        $id = (int)$cliente->idpersona;

        if ($this->getCliente($id)) {
            $this->tableGateway->update(
                $data, array('idcliente' => $id)
            );
        } else {
            //throw new \Exception('Persona no existe');
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        }

        return $id;
    }
    private function resultToArray($result)
    {
        $data = array();
        foreach ($result as $value) {
            $data[] = $value;
        }

        return $data;
    }


}

?>